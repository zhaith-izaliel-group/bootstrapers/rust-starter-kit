# {{PROJECT_NAME}}

## Description

This section should contain a small description of the project.

## Overview

This section should contain an overview of the project with 5 sections

- Directory structure explaining the different files and their purpose.
- Description of every features
- How to contribute to this project
- How to deploy the project (build it for production-ready projects)
- How to use: provide some information to get the user started.

### Directory structure

### Feature description

### How to contribute to this project?

### How to use?

### How to build?

## Other/Optional considerations

This section describes optional or other considerations related to this particular project
